import {Component, OnInit} from '@angular/core';
//import {Component, OnInit, EventEmitter} from '@angular/core';
import {Router}          from "@angular/router";
import {VariablesService, HttpService} from '../services';
import * as $ from 'jquery';
import { LoginComponent } from '../login';

@Component({
  selector: 'new',
  template: require('./new.component.html')
})

export class NewComponent implements OnInit {

     // inputValueChange: EventEmitter<any> = new EventEmitter();
      $ = require('jquery');
      templateids: any=[];
      user_id: any;
      first_name: any;
      last_name: any;
      fromdate: any;
      todate: any;
      plan_id: any=null;
      access: boolean=false;
      template_id: any;
      workoutdetails: any;
      static edit_count: any;
      date: string;
      filenamearray: Array<string>=[];
      plandetails: any;
      create: boolean=false;
      update: boolean=false;
      planexistence: any;
      router:Router;
      static grid_editflag: boolean=true;
      filelength: any;
      filename: string;
      status: any;
      //on tab of user id
      getuserdetails(){

            console.log(this.access);
            
            if(this.user_id==null){
                  alert("Enter user id");
                  return;
            }
            let url=VariablesService.getuserdetails;
            console.log(this.user_id);
            
            let requestbody ={
                  user_id: this.user_id
            }
            HttpService.get(url,requestbody).then(data => {
                  console.log("data"+data);
                  
                  data=JSON.parse(data);
                  if(data.status==false){
                        alert(data.errorMessage);
                        return;
                  }
                  this.first_name=data.first_name;
                  this.last_name=data.last_name;
                  this.access=true;
                  this.create=true;
             });
      }
      logout(){
            LoginComponent.logout();
            this.router.navigate(['login']);
      }
      //on enter of user id 
      getworkoutplanheader(){

            this.create=false;
            if(this.user_id==null){
                  alert("Enter user id");
                  return;
            }

            let url1=VariablesService.getuserdetails;
            let url2=VariablesService.getworkoutplanheader;
            let requestbody ={
                  user_id: this.user_id
            }
            HttpService.get(url1,requestbody).then(data => {
                  console.log("data"+data);
                  
                  data=JSON.parse(data);
                  if(data.status==false){
                        alert(data.errorMessage);
                        return;
                  }
                  this.first_name=data.first_name;
                  this.last_name=data.last_name;

                  HttpService.get(url2,requestbody).then(data1 => {
                        console.log("data"+data1);
                        
                        data1=JSON.parse(data1);
                        if(data1.status==false){
                              alert(data1.errorMessage);
                              return;
                        }
                        this.fromdate=data1.fromdate;
                        this.todate=data1.todate;
                        this.template_id=data1.template_id;
                        this.plan_id=data1.plan_id;
                        this.access=true;
                        this.update=true;

                        let url3=VariablesService.getworkoutplandetails;
                        let requestbody2={
                              user_id:this.user_id,
                              plan_id : this.plan_id
                        }
      
                  //fetching data for workout details grid
                  HttpService.get(url3,requestbody2).then(data2 => {
                        console.log("data"+data2);
                        this.workoutdetails=JSON.parse(data2);
                        if(this.workoutdetails[0].status==false){
                              alert(this.workoutdetails[0].errorMessage);
                              return;    
                        }
                        let url4=VariablesService.planexistencecheck;
                        let requestbody1={
                              user_id :this.user_id,
                              plan_id : this.plan_id,
                        }
                        HttpService.get(url4,requestbody1).then(data => {
                              console.log("data"+data);
                              this.planexistence=JSON.parse(data); 
                              if(this.planexistence.status==false){
                                    alert(this.planexistence.errorMessage);
                                    return;
                              }else if(this.planexistence.count>0){
                                    NewComponent.grid_editflag=false;
                                    return;
                              }
                        });
                        $("#jsGrid").jsGrid("loadData",this.workoutdetails);
                        this.create=false;
                        this.update=true;
                  });

                  });

            });
      }

      fetchplandetails(){
            if(this.fromdate==null){
                  alert("Enter from date");
                  return;
            }
            let url1=VariablesService.getplanid;
            let requestbody={
                  user_id:this.user_id,
                  date : this.fromdate
            }
            console.log("data"+requestbody);
            HttpService.get(url1,requestbody).then(data1 => {
                  console.log("data1"+data1);
                  this.plandetails=JSON.parse(data1);
                  if(this.plandetails.status==false){
                        alert(this.plandetails.errorMessage);
                        return;
                  }
                  this.fromdate=this.plandetails.fromdate;
                  this.todate=this.plandetails.todate;
                  this.template_id=this.plandetails.template_id;
                  this.plan_id=this.plandetails.plan_id;
                  console.log("plan id..."+this.plan_id);
                  this.access=true;   

                  let url2=VariablesService.getworkoutplandetails;
                  let requestbody2={
                        user_id:this.user_id,
                        plan_id : this.plan_id
                  }
      
                  //fetching data for workout details grid
                  HttpService.get(url2,requestbody2).then(data2 => {
                        console.log("data"+data2);
                        this.workoutdetails=JSON.parse(data2);
                        if(this.workoutdetails[0].status==false){
                              alert(this.workoutdetails[0].errorMessage);
                              return;    
                        }
                        let url1=VariablesService.planexistencecheck;
                        let requestbody1={
                              user_id :this.user_id,
                              plan_id : this.plan_id,
                        }
                        HttpService.get(url1,requestbody1).then(data => {
                              console.log("data"+data);
                              this.planexistence=JSON.parse(data); 
                              if(this.planexistence.status==false){
                                    alert(this.planexistence.errorMessage);
                                    return;
                              }else if(this.planexistence.count>0){
                                    NewComponent.grid_editflag=false;
                                    return;
                              }
                        });
                        $("#jsGrid").jsGrid("loadData",this.workoutdetails);
                        this.create=false;
                        this.update=true;
                  });
            });  
      }
      settemplateid($event){
             this.template_id=$event.target.value;
      }
      downloadtemplate(){

            if(this.fromdate==null){
                  this.date = this.getNowDate();
            }else{
                  this.date=this.fromdate;
            }
           
            let url1=VariablesService.getplanid;
            let requestbody1={
                  user_id:this.user_id,
                  date : this.date
            }
            HttpService.get(url1,requestbody1).then(data1 => {
                  console.log("data1"+data1);
                  this.plandetails=JSON.parse(data1);
                  if(this.plandetails.status==false){
                        alert(this.plandetails.errorMessage);
                        return;
                  }
                  this.fromdate=this.plandetails.fromdate;
                  this.todate=this.plandetails.todate;
                  this.template_id=this.plandetails.template_id;
                  this.plan_id=this.plandetails.plan_id;
                  console.log("plan id..."+this.plan_id);
            });
            let url2=VariablesService.downloadtemplate;
            let requestbody2={       
                  user_id: this.user_id,
                  date: this.date,
                  plan_id:this.plan_id
            }
            HttpService.get(url2,requestbody2).then(data2 => {
                  console.log("data"+data2);
                  data2=JSON.parse(data2);
                  alert("Downloaded Filename "+data2.filename);                 
            });
      }

      filechange($event){
            this.filelength=$event.target.files.length;
            for(let i=0;i<this.filelength;i++){
                  this.filenamearray[i]=$event.target.files[i].name;
            }    
      }

      uploadtemplate(){

            let url1=VariablesService.planexistencecheck;
            let requestbody1={
                  user_id :this.user_id,
                  plan_id : this.plan_id,
            }
            // HttpService.get(url1,requestbody1).then(data => {
            //       console.log("data"+data);
            //       this.planexistence=JSON.parse(data); 
            // if(this.planexistence.status==false){
            //       alert(this.planexistence.errorMessage);
            //       return;
            // }else if(this.planexistence.count>0){
            //     alert("Workout Details Exists For This Plan.Editing Plan Not Allowed");
            //     return;
            // }
            let url=VariablesService.uploadtemplate;
            for(let i=0;i<this.filelength;i++){
            this.filename=this.filenamearray[i];
            if(this.filename.includes("summary")){
                  let url2=VariablesService.uploadgarmindataheader;
                  let requestbody={
                        user_id: this.user_id,
                        uploadUserId: 3,
                        userProfileId:75775106,
                        date: this.date,
                        filename : this.filename
                  }
                  HttpService.get(url2,requestbody).then(data2 => {
                        this.status=JSON.parse(data2);
                        // if(this.status.status==false){
                        //       alert("failed"+this.filename)
                        //       return;
                        // }
                  }); 
            }else if(this.filename.includes("details")){
                  let url2=VariablesService.uploadgarmindatadetails;
                  let requestbody={
                        user_id: this.user_id,
                        uploadUserId: 3,
                        userProfileId:75775106,
                        date: this.date,
                        filename : this.filename
                  }
                  HttpService.get(url2,requestbody).then(data2 => {
                        this.status=JSON.parse(data2);
                        // if(this.status.status==false){
                        //       alert("failed"+this.filename)
                        //       return;
                        // }
                  }); 
            }
      }
            // HttpService.get(url,requestbody).then(data2 => {
            //       console.log("data"+data2);
            //       alert("Upload Completed. Check "+data2+" File"); 
            // }); 
            // });

      }
      clear(){

            this.user_id=null;
            this.plan_id=null;
            this.fromdate=null;
            this.todate=null;
            this.access=false;
            this.template_id=null;
            this.workoutdetails=null;
            $("#jsGrid").jsGrid("loadData",this.workoutdetails);
            this.first_name=null;
            this.last_name=null;
            this.create=false;
            this.update=false;
      }

      gettemplateids(){

            let url=VariablesService.gettemplateids;

            HttpService.get(url).then(data => {
                  console.log("data"+data);
                  this.templateids=JSON.parse(data);
                  if(this.templateids[0].status==false){
                        alert(this.templateids[0].errorMessage);
                        return;    
                  }
            });
      }

      maintainworkoutplanheader(){

            if(this.user_id==null){
                  alert("Enter user id");
                  return;
            }else if(this.fromdate==null){
                  alert("Enter fromdate");
                  return;
            }else if(this.todate==null){
                  alert("Enter todate");
                  return;
            }else if(this.todate<=this.fromdate){
                  alert("fromdate should be less than todate");
                  return;
            }
            let url=VariablesService.maintainworkoutplanheader;

            let requestbody={
                  user_id :this.user_id,
                  plan_id : this.plan_id,
                  fromdate:this.fromdate,
                  todate:this.todate,
                  template_id:this.template_id
            }

            HttpService.get(url,requestbody).then(data =>{

                  data = JSON.parse(data);
                  if(data.status == false){
                        alert(data.errorMessage);
                        return;
                  }else if(data.status == true)
                  {
                        this.plan_id=data.plan_id;
                        let url1=VariablesService.getworkoutplandetails;
                        let requestbody={
                              user_id:this.user_id,
                              plan_id : this.plan_id
                        }
                        //fetching data for workout details grid
                        HttpService.get(url1,requestbody).then(data1 => {
                              console.log("data"+data1);
                              this.workoutdetails=JSON.parse(data1);
                              if(this.workoutdetails[0].status==false){
                                    alert(this.workoutdetails[0].errorMessage);
                                    return;    
                              }
                              $("#jsGrid").jsGrid("loadData",this.workoutdetails);   
                        });
                  }
            });
      }

      grid_submit(){
            let url1=VariablesService.planexistencecheck;
            let requestbody1={
                  user_id :this.user_id,
                  plan_id : this.plan_id,
            }
            HttpService.get(url1,requestbody1).then(data => {
                  console.log("data"+data);
                  this.planexistence=JSON.parse(data);  
            if(this.planexistence.status==false){
                  alert(this.planexistence.errorMessage);
                  return;
            }else if(this.planexistence.count>0){
                alert("Workout Details Exists For This Plan.Editing Plan Not Allowed");
                return;
            }
            var grid_data = JSON.stringify(this.workoutdetails);
            console.log("grid details"+grid_data);
            let url=VariablesService.grid_submit;
            let requestbody={
                  data : grid_data,
                  user_id :this.user_id,
                  plan_id : this.plan_id,
                  fromdate:this.fromdate,
                  todate:this.todate,
                  template_id:this.template_id
            }
            HttpService.get(url,requestbody).then(data1 =>{
                  console.log("data"+data1);
                  data1 = JSON.parse(data1);
                  if(data1.status==false){
                       alert(data1.errorMessage);return;
                  }
                  this.fetchplandetails();
            });   
            });
      }

      getNowDate() {
            //return string
            var returnDate = "";
            //get datetime now
            var today = new Date();
            //split
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //because January is 0! 
            var yyyy = today.getFullYear();
            returnDate +=yyyy;
            returnDate +='-';
            //Interpolation date
            if (mm < 10) {
              returnDate += `0${mm}-`;
          } else {
              returnDate += `${mm}-`;
          }
            if (dd < 10) {
                returnDate += `0${dd}`;
            } else {
                returnDate += `${dd}`;
            }  
            return returnDate;
        } 

      ngAfterViewInit(){
          var day = new Date();
          var month = day.getMonth()+1;
          var date = day.getDate();

         
          
      }


      ngOnInit(){
            var a = this;
            console.log('New component loaded...');
            console.log("JSGrid refreshed...");

            this.gettemplateids();

            $(function(){
                  var day = new Date();
                  var month = day.getMonth()+1;
                  var date = day.getDate();
                  var year = day.getFullYear();

                  if(month < 10){
                  }
                  else{
                  }

                  $("#jsGrid").jsGrid({
                           height: "40vh",
                           width: "100%",

                           editing: true,
                           paging: false,
                           autoload:true,
                           data: {},
                           onItemDeleted: function() {
                           } ,
                           
                           controller:{
                                 loadData: function(items){
                                       return items;
                                 },
                                 insertItem: function (item){
                                          return item;
                                 },
                                 deleteItem: function (item){
                                     return item;
                                 }
                           },

                           rowClick: function(args){
                              this.editing=NewComponent.grid_editflag;    
                              if(this.editing) {

                                    this.editItem($(args.event.target).closest("tr"));
                                    NewComponent.edit_count = args.itemIndex;
                                    var type = a.workoutdetails[NewComponent.edit_count]["exercise_type"];
                                    if(type==1){
                                          console.log("1------>"+type);
                                          $("#jsGrid").jsGrid("editItem",a.workoutdetails[NewComponent.edit_count]);
                                          $(".jsgrid-edit-row input:eq(0)").focus();
                                          $(".jsgrid-edit-row input:eq(0)").select();

                                    }else if(type==2){
                                          console.log("2-------->"+type);
                                          $("#jsGrid").jsGrid("editItem",a.workoutdetails[NewComponent.edit_count]);
                                          $(".jsgrid-edit-row input:eq(1)").focus();
                                          $(".jsgrid-edit-row input:eq(1)").select();
                                    }
                              }
                                         
                           },
                           
                           fields: [
                                    { name: "dayno", title: "Day No",type:"readonly" ,align:"center"},
                                    { name: "exercise_name", title: "Exercise Name", type: "readonly",align:"center"},
                                    { name: "exercise_type", title: "Exercise Type", type: "readonly",align:"center"},
                                    // { name: "item_id", title: "Item", type: "readonly",align:"center"},
                                    { name: "intensity", title: "Intensity", type: "readonly", align:"center"},
                                    { name: "duration", title: "Duration",type: "number",align:"center"},
                                    { name: "repetition", title:"Repetition", type: "number",align:"center"},
                                    { name: "sets", title:"sets",type: "number",align:"center"},
                                    {name:"isEdited",type:"boolean",visible:false}
                          ],

                        //   rowClass: function(item, itemIndex) {
                        //         return item.Margin_Difference >= 0 ?  "correct-row" : "incorrect-row";
                        //   }
                  });
                  $('.jsgrid-table').on("keyup", function(e)
                  {

                       
                        if(e.which==13)
                        {
                              e.preventDefault();
                              $("#jsGrid").jsGrid("updateItem").then(function()
                              {     
                                    a.workoutdetails[NewComponent.edit_count]["isEdited"]=true;
                                    NewComponent.edit_count=NewComponent.edit_count+1;
                                    if(NewComponent.edit_count==(a.workoutdetails.length))NewComponent.edit_count=0;
                                    var type = a.workoutdetails[NewComponent.edit_count]["exercise_type"];
                              if(type==1){
                                    console.log("1------>"+type);
                                    $("#jsGrid").jsGrid("editItem",a.workoutdetails[NewComponent.edit_count]);
                                    $(".jsgrid-edit-row input:eq(0)").focus();
                                    $(".jsgrid-edit-row input:eq(0)").select();   
                              }else if(type==2){
                                    console.log("2-------->"+type);
                                    $("#jsGrid").jsGrid("editItem",a.workoutdetails[NewComponent.edit_count]);
                                    $(".jsgrid-edit-row input:eq(1)").focus();
                                    $(".jsgrid-edit-row input:eq(1)").select();
                              }

                              });
                        }
                  });
                  

            });
      }
}
