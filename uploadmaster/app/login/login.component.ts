import {Component, OnInit} from '@angular/core';
import {Router, Params} from "@angular/router";
import * as $ from 'jquery';


import {VariablesService, CookiesService, HttpService} from '../services';
import { ANY_STATE } from '@angular/core/src/animation/animation_constants';

@Component({
  selector: 'login',
  template: require('./login.component.html'),
  providers: [CookiesService, VariablesService]
})

export class LoginComponent{
     

      user_id:any;
      password:any;
      static login_status: boolean;
      ou_id: any;
      token:any;


      constructor(public router: Router) {
            let url=VariablesService.validateToken;
           
            HttpService.get(url).then(data => {
                  data=JSON.parse(data);
                  if(data.status==true){
                        this.router.navigate(['new']);      
                  }else{
                        this.router.navigate(['login']);
                  }
            });
      }

      login(){
            
            let url=VariablesService.Login_Validate;
            console.log(this.user_id);
            let requestbody ={
                  user_id: this.user_id,
                  password:this.password,
                  ou_id: this.ou_id
            }
            console.log(requestbody);
            HttpService.get(url,requestbody).then(data => {
                  data=JSON.parse(data);
                  console.log("data"+data.Token);
                  if(data.status==true){
                  VariablesService.loggedin_user_id=this.user_id;
                  CookiesService.set_cookie(VariablesService.Token_Name,data.Token);
                  LoginComponent.login_status = true;
                  this.router.navigate(['new',{logged_in_user_id : this.user_id}]);
                  }
             });
      }

      static logout(){

            let url=VariablesService.logout;


            HttpService.get(url).then(data => {
                  data=JSON.parse(data);
                  if(data.status==true){
                        console.log("Success");
                        LoginComponent.login_status = false;
                        CookiesService.delete_cookie(VariablesService.Token_Name);
                  }
            });
            
      }
}
