import {Component} from '@angular/core';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  template: require('./app.component.html')
})
export class AppComponent {
  name: string = 'Brunch for Angular 2';
  url: string = 'http://colin.is/blog';
  constructor() {

  }
}
