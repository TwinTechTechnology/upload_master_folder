import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule}   from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {NewComponent} from './new';
import {routing} from './app.routes';

import {VariablesService, AuthenticationService, SignoutService, CookiesService, HttpService, ValidateDateService} from './services';
import { LoginComponent } from './login';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        routing
    ],
    declarations: [
        AppComponent,
        NewComponent,
        LoginComponent
    ],
    providers: [
          AuthenticationService,
          VariablesService,
          SignoutService,
          CookiesService,
          HttpService,
          ValidateDateService
   ],
    bootstrap: [AppComponent]
})
export class AppModule {}
