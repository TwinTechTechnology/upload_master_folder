import {Routes, RouterModule} from '@angular/router';

import {NewComponent} from './new';
import { LoginComponent } from './login/login.component';

export const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'new', component: NewComponent},
  {path: 'login', component: LoginComponent},
];

export const routing = RouterModule.forRoot(routes);
