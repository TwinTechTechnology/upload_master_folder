import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as $ from 'jquery';

@Injectable()
export class CookiesService {
    constructor(public router: Router) {
        CookiesService.get_all_cookies();
    }

    public static integratr_cookies = {};

    public static get_all_cookies:any = function(){
        CookiesService.integratr_cookies = {};

        var cookies = document.cookie.split('; ');
        for(var i=0; i<cookies.length ; i++){
            var cookie = cookies[i].split('=');
            CookiesService.integratr_cookies[cookie[0]] = cookie[1];
        }
    }

    public static set_cookie:any = function(name:string, value:string){
        document.cookie = name+"="+value+";path=/";
        console.log("success");
        
    }

    public static get_cookie:any = function(cookie: string){
        CookiesService.get_all_cookies();
        return Promise.resolve(CookiesService.integratr_cookies[cookie]);
    }

    public static delete_cookie:any = function(name: string){
        document.cookie = name+"="+null+";max-age=-1;path=/";
    }
}
