import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { VariablesService, CookiesService } from '../services';
import * as $ from 'jquery';

@Injectable()
export class AuthenticationService implements CanActivate {
    constructor(public router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        CookiesService.get_all_cookies();
        if (VariablesService.Token_Name in CookiesService.integratr_cookies){
            // logged in so return true
            if(!VariablesService.activity_check){
                $.ajax({
                    type: "POST",
                    url: VariablesService.Common_Get_Activivty_Mapping,
                    async: false,
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function(data){
                        var obj = $.parseJSON(data);
                        VariablesService.Menu_Item_Master = obj['Menu Item Master'];
                        VariablesService.Menu_Vendor_Master = obj['Menu Vendor Master'];
                        VariablesService.Menu_Item_Mapping = obj['Menu Item Mapping'];
                        VariablesService.Menu_Item_EAN_Mapping = obj['Menu Item EAN Mapping'];
                        VariablesService.Menu_Master_Excel_Upload = obj['Menu Master Excel Upload'];
                        VariablesService.Menu_GRN = obj['Menu GRN'];
                        VariablesService.Menu_Returns_Entry = obj['Menu Returns Entry'];
                        VariablesService.Menu_Returns_Complete = obj['Menu Returns Complete'];
                        VariablesService.Menu_Actual_Returns = obj['Menu Actual Returns'];
                        VariablesService.Menu_Store_Stock_Details = obj['Menu Store Stock Details'];
                        VariablesService.Menu_GRN_Report = obj['Menu GRN Report'];
                        VariablesService.Menu_Item_Report = obj['Menu Item Report'];
                        VariablesService.Menu_GRN_Excel_Report = obj['Menu GRN Excel Report'];
                        VariablesService.Menu_Merchandise_Report = obj['Menu Merchandise Report'];
                        VariablesService.Menu_GRN_Recompute = obj['Menu GRN Recompute'];
                        VariablesService.Item_Master_New = obj['Item Master New'];
                        VariablesService.Item_Master_Edit = obj['Item Master Edit'];
                        VariablesService.Item_Master_View = obj['Item Master View'];
                        VariablesService.Vendor_Master_New = obj['Vendor Master New'];
                        VariablesService.Vendor_Master_Edit = obj['Vendor Master Edit'];
                        VariablesService.Vendor_Master_View = obj['Vendor Master View'];
                        VariablesService.Item_EAN_Mapping_View = obj['Item EAN Mapping View'];
                        VariablesService.Item_EAN_Mapping_View_and_New = obj['Item EAN Mapping View and New'];
                        VariablesService.Master_Excel_Upload_for_Item_Margin = obj['Master Excel Upload for Item Margin'];
                        VariablesService.Master_Excel_Upload_for_Item_GST = obj['Master Excel Upload for Item GST'];
                        VariablesService.Master_Excel_Upload_for_Item_Vendor = obj['Master Excel Upload for Item Vendor'];
                        VariablesService.Master_Excel_Upload_for_Item_MRP = obj['Master Excel Upload for Item MRP'];
                        VariablesService.GRN_Recompute = obj['GRN Recompute'];
                        VariablesService.GRN_Recompute_Upload_Margin = obj['GRN Recompute Upload Margin'];
                        VariablesService.GRN_Recompute_Upload_GST = obj['GRN Recompute Upload GST'];
                        VariablesService.GRN_Recompute_Upload_Vendor = obj['GRN Recompute Upload Vendor'];
                        VariablesService.GRN_Recompute_Upload_Force_Complete = obj['GRN Recompute Upload Force Complete'];
                        VariablesService.GRN_New = obj['GRN New'];
                        VariablesService.GRN_Edit = obj['GRN Edit'];
                        VariablesService.GRN_View = obj['GRN View'];
                        VariablesService.Store_Stock_Details_New = obj['Store Stock Details New'];
                        VariablesService.Store_Stock_Details_Edit = obj['Store Stock Details Edit'];
                        VariablesService.Store_Stock_Details_View = obj['Store Stock Details View'];
                        VariablesService.Returns_Entry_New = obj['Returns Entry New'];
                        VariablesService.Returns_Entry_Edit = obj['Returns Entry Edit'];
                        VariablesService.Returns_Entry_View = obj['Returns Entry View'];
                        VariablesService.Returns_Complete_Draft = obj['Returns Complete Draft'];
                        VariablesService.Returns_Complete_Complete = obj['Returns Complete Complete'];
                        VariablesService.Returns_Complete_Dumped = obj['Returns Complete Dumped'];
                        VariablesService.Returns_Complete_Upload_Margin = obj['Returns Complete Upload Margin'];
                        VariablesService.Returns_Complete_Upload_Vendor = obj['Returns Complete Upload Vendor'];
                        VariablesService.Returns_Complete_Upload_GST = obj['Returns Complete Upload GST'];
                        VariablesService.Actaul_Returns_New = obj['Actaul Returns New'];
                        VariablesService.Actaul_Returns_Edit = obj['Actaul Returns Edit'];
                        VariablesService.Actaul_Returns_View = obj['Actaul Returns View'];
                        VariablesService.activity_check = true;
                    },
                    error: function(){
                        console.log("Something went wrong");
                    }
                });
            }

            switch(state['url']){
                case '/item': if(!VariablesService.Menu_Item_Master){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/vendor': if(!VariablesService.Menu_Vendor_Master){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/item_mapping': if(!VariablesService.Menu_Item_Mapping){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/ean_mapping': if(!VariablesService.Menu_Item_EAN_Mapping){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/upload_master_excel': if(!VariablesService.Menu_Master_Excel_Upload){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/new': if(!VariablesService.Menu_GRN){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/new_returns': if(!VariablesService.Menu_Returns_Entry){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/returns_compute': if(!VariablesService.Menu_Returns_Complete){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/actual_returns': if(!VariablesService.Menu_Actual_Returns){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/stock_details': if(!VariablesService.Menu_Store_Stock_Details){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/view': if(!VariablesService.Menu_GRN_Report){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/item_report': if(!VariablesService.Menu_Item_Report){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/excel': if(!VariablesService.Menu_GRN_Excel_Report){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/merchandise': if(!VariablesService.Menu_Merchandise_Report){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
                case '/recompute': if(!VariablesService.Menu_GRN_Recompute){
                                    this.router.navigate(['menu']);
                                    return false;
                              }
                              break;
            }

            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['login']);
        return false;
    }
}
