import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { VariablesService, CookiesService } from '../services';
import * as $ from 'jquery';

@Injectable()
export class SignoutService {
    constructor(public router: Router) {

   }
   redirect:any = function(destination: string){
       this.router.navigate([destination]);
   }

      public static signout:any = function(){
            var flag = false;
            if(!navigator.onLine){
                  alert("Check Your Internet Connection...After Signout Your Account");
                  return;
            }
            $.ajax({
                  type: "GET",
                  url: VariablesService.Common_URL + "delete_token",
                  async: false,
                  beforeSend: function(xhr){
                        xhr.setRequestHeader('token', CookiesService.get_cookie(VariablesService.Token_Name));
                  },
                  success: function(){
                        flag = true;
                        CookiesService.delete_cookie(VariablesService.Token_Name);
                        VariablesService.activity_check = false;
                  },
                  error: function(){
                        alert("Something went wrong");
                  }
            });
            return flag;
      }
}
