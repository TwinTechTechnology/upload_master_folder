import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Injectable()
export class ValidateDateService {
    constructor(public router: Router) {

    }

    public static validateDOB:any = function(dob:any){
          let dd;
          let mm;
          let yy;
                if(dob == ""){
                    return Promise.resolve([true,dob]);
                }
                let data = dob.split("-");
                if (data.length <2) {
                      data = dob.split("/");
                      if (data.length <2) {
                            data = dob.split(".");
                            if (data.length <2) {
                                  if (dob.length == 8){
                                        dd= parseInt(dob.substring(0,2));
                                        mm= parseInt(dob.substring(2,4));
                                        yy= parseInt(dob.substring(4,8));
                                        data[0]=dob.substring(0,2);
                                        data[1]=dob.substring(2,4);
                                        data[2]=dob.substring(4,8);
                                  }else if (dob.length == 6) {
                                        dd= parseInt(dob.substring(0,2));
                                        mm= parseInt(dob.substring(2,4));
                                        yy= parseInt(dob.substring(4,6));
                                        if(mm==20){
                                              mm=dd;
                                              dd=1;
                                              yy=parseInt(dob.substring(2,6));
                                              data[0]="01";
                                              data[1]=dob.substring(0,2);
                                              data[2]=dob.substring(2,6);
                                        }else{
                                            data[0]=dob.substring(0,2);
                                            data[1]=dob.substring(2,4);
                                            data[2]=dob.substring(4,6);
                                        }
                                  }else if (dob.length == 4){
                                        dd= 1;
                                        mm= parseInt(dob.substring(0,2));
                                        yy= parseInt(dob.substring(2,4));
                                        data[0]="01";
                                        data[1]=dob.substring(0,2);
                                        data[2]=dob.substring(2,4);
                                  }else if (dob.length == 3){
                                        dd= 1;
                                        mm= parseInt(dob.substring(0,1));
                                        yy= parseInt(dob.substring(1,3));
                                        data[0]="01";
                                        data[1]=dob.substring(0,1);
                                        data[2]=dob.substring(1,3);
                                  } else {
                                      return Promise.resolve([false,dob]);
                                  }
                            } else{
                                  if (data.length==2){
                                        dd=1;
                                        mm= parseInt(data[0]);
                                        yy=parseInt(data[1]);

                                        data[2]=data[1];
                                        data[1]=data[0];
                                        data[0]="01";
                                  } else{
                                      dd = parseInt(data[0]);
                                      mm = parseInt(data[1]);
                                      yy = parseInt(data[2]);
                                  }
                            }
                      } else {
                          if (data.length==2){
                                dd=1;
                                mm= parseInt(data[0]);
                                yy=parseInt(data[1]);
                                data[2]=data[1];
                                data[1]=data[0];
                                data[0]="01";
                          } else{
                              dd = parseInt(data[0]);
                              mm = parseInt(data[1]);
                              yy = parseInt(data[2]);
                          }
                      }
                } else {
                      if (data.length==2){
                            dd=1;
                            mm= parseInt(data[0]);
                            yy=parseInt(data[1]);

                            data[2]=data[1];
                            data[1]=data[0];
                            data[0]="01";
                      } else{
                          dd = parseInt(data[0]);
                          mm = parseInt(data[1]);
                          yy = parseInt(data[2]);
                      }
                }
          let date;
          let rdate;
          if (yy < 100) {
                yy+=2000;
                date = yy + "-" + data[1] + "-" + data[0];
                rdate=data[0]+"-"+data[1]+"-"+yy;
          }else{
              date = data[2] + "-" + data[1] + "-" + data[0];
              rdate=data[0]+"-"+data[1]+"-"+data[2];
          }
          if (isNaN(Date.parse(date)) || mm > 12 || yy<2000) {
                return Promise.resolve([false,dob]);
          }
          return Promise.resolve([true,rdate]);
    }
}
