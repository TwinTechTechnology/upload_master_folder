import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Injectable()
export class HttpService {
    constructor(public router: Router) {

    }

    public static get:any = function(url:any, data:any = {}){
        let response:any;
        $.ajax({
            type: "GET",
            url: url,
            async: false,
            data: data,
            xhrFields: {
                withCredentials: true
            },
            success: function(data){
                    response = data;
            },
            error: function(){
                    response = "Error";
            }
        });
        return  Promise.resolve(response);
    }

    public static post:any = function(url:any, data:any = {}){
        let response:any;
        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: data,
            xhrFields: {
                withCredentials: true
            },
            success: function(data){
                    response = data;
            },
            error: function(){
                    response = "Error";
            }
        });
        return  Promise.resolve(response);
    }
}
