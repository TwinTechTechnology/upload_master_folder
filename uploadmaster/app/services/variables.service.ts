import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class VariablesService {
  public static urls = "http://192.168.1.11:8080/sixalpha_application/";
  //Login
  public static Login_Validate = VariablesService.urls + "login/login/angularLoginValidation";
  public static validateToken = VariablesService.urls + "login/login/angularValidateToken";
  public static logout = VariablesService.urls + "login/login/angularDeleteToken";
  //Common
  public static Common_URL = VariablesService.urls + "common/common_methods/";
  //Transactions
  public static getworkoutplandetails = VariablesService.urls + "uploadmaster/downloadTemplate/getWorkoutPlanDetails";
  public static downloadtemplate = VariablesService.urls + "uploadmaster/downloadTemplate/downloadTemplateFile";
  public static uploadtemplate = VariablesService.urls + "uploadmaster/downloadTemplate/uploadTemplateFile";
  //garmin data upload
  public static uploadgarmindataheader = VariablesService.urls + "uploadmaster/downloadTemplate/uploadGarminDataHeader";
  public static uploadgarmindatadetails = VariablesService.urls + "uploadmaster/downloadTemplate/uploadGarminDataDetails";
  public static grid_submit = VariablesService.urls + "uploadmaster/downloadTemplate/gridSubmit";
  public static maintainworkoutplanheader = VariablesService.urls + "uploadmaster/downloadTemplate/maintainWorkoutPlanHeader";
  public static getworkoutplanheader = VariablesService.urls +  "uploadmaster/downloadTemplate/getWorkoutPlanHeader";
  public static getuserdetails = VariablesService.urls +"uploadmaster/downloadTemplate/getUserDetails";
  public static gettemplateids = VariablesService.urls + "uploadmaster/downloadTemplate/getTemplateIds";
  public static getplanid = VariablesService.urls+"uploadmaster/downloadTemplate/getPlanId";
  public static planexistencecheck= VariablesService.urls + "uploadmaster/downloadTemplate/checkPlanExistence";
  static Token_Name: any="uploadtoken";
  static loggedin_user_id: any;

 
}
