exports.config = {
  // See http://brunch.io/#documentation for docs
  server: { hostname: '0.0.0.0' },
  files: {
    javascripts: {
      joinTo: {
        'vendor.js': /^node_modules/,
        'main.js': /^app/
      },
      order: {
        after: [/\.html$/, /\.css$/]
      }
    },
    stylesheets: {
      joinTo: 'app.css'
    },
    templates: {
      joinTo: 'main.js'
    }
  },
  plugins: {
    inlineCss: {
      html: true,
      passthrough: [/^node_modules/, 'app/global.css']
    },
    brunchTypescript: {
      ignoreErrors: true
      },
      sass: {
            options: {
              includePaths: ["node_modules/bootstrap-sass/assets/stylesheets"], // tell sass-brunch where to look for files to @import
              precision: 8 // minimum precision required by bootstrap-sass
            }
          },
     copycat: {
            "fonts": ["node_modules/bootstrap-sass/assets/fonts/bootstrap"] // copy node_modules/bootstrap-sass/assets/fonts/bootstrap/* to priv/static/fonts/
     }
    },
    modules: {
    autoRequire: {
     "js/app.js": [
       "bootstrap-sass", // require bootstrap-sass' JavaScript globally
       "web/static/js/app"
     ]
    }
    },
    npm: {
    enabled: true,
    whitelist: ["jquery", "bootstrap-sass"], // pull jquery and bootstrap-sass in as front-end assets
    globals: { // bootstrap-sass' JavaScript requires both '$' and 'jQuery' in global scope
     $: 'jquery',
     jQuery: 'jquery'
    }
    }
    };
