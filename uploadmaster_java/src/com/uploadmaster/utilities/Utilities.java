package com.uploadmaster.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {
	public static String dateTime(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'-'HH-mm-ss");
		Date date = new Date();
		String dateTime = dateFormat.format(date);
		return dateTime;
	}
	public static String  ConvertDate(Date date){
		Date mydate = date;
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MM/dd/yyyy");
		String returnDate = mdyFormat.format(mydate);
		return returnDate;
	}
}
