package com.uploadmaster.DB;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

	public static Connection createConnection_mysql(){	
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/six_alpha";
		String username = "root";
		String password = "password";
			try{
				try{
					Class.forName("com.mysql.jdbc.Driver");
				}catch (ClassNotFoundException e){
					e.printStackTrace();
				}
				con = DriverManager.getConnection(url, username, password);
			}catch (Exception e){
				e.printStackTrace();
			}
		return con; 
	}
}
