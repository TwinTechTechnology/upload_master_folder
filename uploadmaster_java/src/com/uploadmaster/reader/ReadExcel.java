package com.uploadmaster.reader;


import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

@SuppressWarnings("unused")
public class ReadExcel {

    private String inputFile;
    private String contents[][];
    private int nrows=0, ncols=0;
    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }
    public String[][] getcontents(){
    	return contents;
    }
    public int getcols(){
    	return ncols;
    }
    public int getrows(){
    	return nrows;
    }
    public void read() throws IOException  {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        try {
            w = Workbook.getWorkbook(inputWorkbook);
            // Get the first sheet
            Sheet sheet = w.getSheet(0);
            // Loop over first 10 column and lines
            nrows=sheet.getRows();
            ncols=sheet.getColumns();
            System.out.println("rows....."+nrows+"     cols......"+ncols);
            contents=new String[ncols][nrows];
            System.out.println("contents: ");
                for (int i = 0; i < nrows; i++) {
                    for (int j = 0; j < ncols; j++) {
                    Cell cell = sheet.getCell(j, i);
                    //CellType type = cell.getType();
                    //if (type == CellType.LABEL) {
                        contents[j][i]=cell.getContents();
                        System.out.println(contents[j][i]+" ");
                    //}
                }
            }
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }
}
