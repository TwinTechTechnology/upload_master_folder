package com.uploadmaster.login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.uploadmaster.DB.DBConnection;
import com.uploadmaster.utilities.Utilities;



@Path("/login")
public class Login {
	
	static Logger logger = Logger.getLogger(Login.class);
	static List<JsonWebKey> jwkList = null;
	static{
		jwkList = new LinkedList<>();
		for (int kid = 1; kid <= 3; kid++) { 
			JsonWebKey jwk = null;
			try {
				jwk = RsaJwkGenerator.generateJwk(2048);
				} catch (JoseException e) {
				e.printStackTrace();
			} 
			jwk.setKeyId(String.valueOf(kid));  
			jwkList.add(jwk); 
		}
	}
	
	@SuppressWarnings("unchecked")
	@Path("/loginValidation")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String loginValidation(@QueryParam("user_id") String userId,@QueryParam("password") String password,@QueryParam("ouId") String ouId){
		System.out.println("login validation");
		JSONObject obj = new JSONObject();
		String  validateQuery = "select a.user_id as USER_ID,a.first_name as FIRST_NAME,a.last_name as LAST_NAME,a.password as PASSWORD,b.role_id as role_id from users a "
								  + "inner join user_role_mapping b on a.user_id=b.user_id "
				                  + "where a.USER_ID = ? and a.PASSWORD = ? and b.ou_id=?";
		String insertTokens = "insert into token (USER_ID,TOKEN,ou_id,CREATED_DATE,CREATED_BY) values(?,?,?,?,?)";
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				try{
					PreparedStatement ps = con.prepareStatement(validateQuery);			
					try{
						ps.setString(1, userId);
						ps.setString(2, password);
						ps.setString(3, ouId);
						ResultSet rs = ps.executeQuery();
						System.out.println("validation...."+ps);
						try{
							if(rs.next()){
								userId = rs.getString("USER_ID");
								String uname = rs.getString("FIRST_NAME");
								obj.put("user_id", userId);
								obj.put("first_name", uname);
								obj.put("last_name", rs.getString("LAST_NAME"));
								obj.put("password", rs.getString("PASSWORD"));
								obj.put("role_id", rs.getString("role_id"));
								RsaJsonWebKey senderJwk = (RsaJsonWebKey) jwkList.get(0);
								senderJwk.setKeyId("1");
								
								JwtClaims claims = new JwtClaims();
								claims.setIssuer("ananth");
								claims.setExpirationTimeMinutesInTheFuture(30);
								claims.setGeneratedJwtId();
								claims.setIssuedAtToNow();
								claims.setNotBeforeMinutesInThePast(2);
								claims.setSubject(userId);
								claims.setStringListClaim("roles", "admin");
								
								JsonWebSignature jws = new JsonWebSignature();
								
								jws.setPayload(claims.toJson());
								jws.setKeyIdHeaderValue(senderJwk.getKeyId());
								jws.setKey(senderJwk.getPrivateKey());
								jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
								
								String jwt = null;
								try{
									jwt = jws.getCompactSerialization();
								}catch(Exception e){
									e.printStackTrace();
								}
								
								PreparedStatement ps1 = con.prepareStatement(insertTokens);
								try{
									ps1.setString(1, userId);
									ps1.setString(2, jwt);
									ps1.setString(3, ouId);
									ps1.setString(4, Utilities.dateTime());
									ps1.setString(5, userId);
									try{
										ps1.executeUpdate();
									} finally {
										ps1.close();
									}
								}finally{
									ps1.close();
								}
								obj.put("Token", jwt);
							}
						}finally{
							rs.close();
						}
					}finally{
						ps.close();
					}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return obj.toString();
	}
	
	@SuppressWarnings("unchecked")
	@Path("/validateToken")
	@PUT
	@Produces(MediaType.TEXT_PLAIN)
	public static String validateToken(String data){
		JSONParser parser = new JSONParser();
		JSONObject obj = new JSONObject();
		String validateToken = "select * from token where user_id = ? and token = ?";
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				try{
					PreparedStatement ps = con.prepareStatement(validateToken);
					try{
						JSONObject json = (JSONObject) parser.parse(data);
						ps.setString(1, json.get("user_id").toString());
						ps.setString(2, json.get("token").toString());
						ResultSet rs = ps.executeQuery();
						try{
							if(rs.next()){
								obj.put("status", true);
							}
						}finally{
							rs.close();
						}
					}finally{
						ps.close();
					}
					con.commit();
				}catch(SQLException e){
					obj.put("status", false);
					con.rollback();
					e.printStackTrace();
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			obj.put("status", false);
			e.printStackTrace();
		}
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	@Path("/deleteToken")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static Object deleteToken(@CookieParam("uploadtoken") String token){
		JSONObject obj = new JSONObject();
		String deleteToken = "delete from token where token = ?";
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				try{
					PreparedStatement ps = con.prepareStatement(deleteToken);
					try{
						ps.setString(1, token);
						ps.execute();
					}finally{
						ps.close();
					}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
					obj.put("value","0");
					return obj.toString();
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			obj.put("value","0");
			return obj.toString();
		}
		obj.put("value","1");
		return obj.toString();
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	@Path("/authenticateUserPrivileges")
	@PUT
	@Produces(MediaType.TEXT_PLAIN)
	public static String authenticateUserPrivileges(String data) throws ParseException{
		System.out.println("authenticate....."+data);
		String authenticateRequestQuery = "select a.ou_id, a.user_id, b.role_id, c.activity_id, d.database_name from token as a join user_role_mapping as b on a.ou_id = b.ou_id and a.user_id = b.user_id "
				+ "join role_activity_mapping as c on a.ou_id = c.ou_id and b.role_id = c.role_id join database_mapping as d on a.ou_id = d.ou_id where a.token = ? and c.activity_id = ?";
		JSONObject jsonObj = new JSONObject();
		JSONParser parser = new JSONParser();
		JSONObject obj = new JSONObject();
		JSONObject json = (JSONObject) parser.parse(data);
		String token = json.get("token").toString();
		String activityId = json.get("activity_id").toString();
		jsonObj.put("Status", false);
		Boolean error = false;
		jsonObj.put("Error", false);
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				try{
					PreparedStatement ps = con.prepareStatement(authenticateRequestQuery);
					try{
						ps.setString(1, token);
						ps.setString(2, activityId);
						ResultSet rs = ps.executeQuery();
						try{
							if(rs.next()){
								jsonObj.put("Status", true);
								jsonObj.put("ou_id", rs.getString("ou_id"));
								jsonObj.put("user_id", rs.getString("user_id"));
								jsonObj.put("role_id", rs.getString("role_id"));
								jsonObj.put("DB_Name", rs.getString("database_name"));
							}
						}finally{
							rs.close();
						}
					}finally{
						ps.close();
					}
				}catch(SQLException e){
					e.printStackTrace();
					error = true;
					con.rollback();
					jsonObj.put("Error", true);
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			error = true;
			e.printStackTrace();
			jsonObj.put("Error", true);
		}
		return jsonObj.toString();
	}
	
}
