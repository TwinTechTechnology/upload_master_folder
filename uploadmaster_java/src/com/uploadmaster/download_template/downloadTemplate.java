package com.uploadmaster.download_template;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.uploadmaster.DB.DBConnection;
import com.uploadmaster.login.Login;
import com.uploadmaster.reader.*;
import com.uploadmaster.utilities.Utilities;
import com.uploadmaster.writer.WriteExcel;

import jxl.write.WriteException;

@SuppressWarnings("unused")
@Path("/downloadTemplate")

public class downloadTemplate {

	@SuppressWarnings("unchecked")
	@Path("/downloadTemplateFile")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String downloadTemplateFile(@CookieParam("uploadtoken") String token,@QueryParam("user_id") String userId,@QueryParam("date") String date) throws IOException, WriteException, ParseException{
		
		String loggedUserId;
		JSONParser parser = new JSONParser();
		JSONObject obj = new JSONObject();
		obj.put("activity_id", "DTFT");
		obj.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		
		System.out.println(userId+"  "+date);
//		ReadExcel test = new ReadExcel();
		String fileName ="E:/projects/SIX ALPHA/";
		String dateTime = Utilities.dateTime();
		fileName = fileName+userId+"_"+dateTime+".xls";
//		test.setInputFile(filename);
//        test.read();
		//JSONParser parser = new JSONParser();
		//JSONObject json = (JSONObject) parser.parse(data);
		String getCount="select count(*) as count from user_workout_plan_details where user_id=? and plan_id=(select plan_id from user_workout_plan_header where fromdate  <= ? and todate >= ?)";
		String header[] = {"plan_id","user_id","template_id","dayno","exercise_id","item_id","intensity",
							"exercise_name", "exercise_type","duration","repetition","sets"};
		String contents[][] = new String[12][100];
		String workoutDetails="select a.plan_id,a.user_id,a.template_id,dayno,a.exercise_id,a.item_id,"
				+ "b.intensity,b.exercise_name,b.exercise_type,a.duration,a.repetition,a.sets from "
				+ "user_workout_plan_details a  inner join exercise_definition b "
				+ "on a.exercise_id=b.exercise_id where a.user_id=? and a.plan_id=(select plan_id from user_workout_plan_header where fromdate  <= ? and todate >= ?)";
		String contentsQuery="select plan_id,a.user_id,a.template_id,dayno,b.exercise_id,b.item_id,"
				+ "c.intensity,c.exercise_name,c.exercise_type from "
				+ "user_workout_plan_header a inner join workout_template b on"
				+ " a.template_id=b.template_id inner join exercise_definition c "
				+ "on b.exercise_id=c.exercise_id where a.user_id=? and fromdate  <= ? and todate >= ?";
		int row=0,col=0;
		int count=0;
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				try{
					PreparedStatement ps = con.prepareStatement(getCount);
					try{
						ps.setString(1, userId);
						ps.setString(2, date);
						ps.setString(3, date);
						ResultSet rs = ps.executeQuery();
						try{
							if(rs.next()){
								count=rs.getInt("count");
							}
						}finally{
							rs.close();
						}
					}finally{
						ps.close();
					}
				con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
				
				if(count==0)
				{
					try{
						PreparedStatement ps = con.prepareStatement(contentsQuery);
						try{
							
							ps.setString(1, userId);
							ps.setString(2, date);
							ps.setString(3, date);
							System.out.println("contents query...."+ps);
							ResultSet rs = ps.executeQuery();
							try{
								while(rs.next()){
									for(col=0;col<9;col++){
										System.out.println(header[col]+"    ");
										System.out.println(rs.getString(header[col]));
										contents[col][row]=rs.getString(header[col]);
									}
									row++;
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
						con.commit();
					}catch(SQLException e){
						con.rollback();
						e.printStackTrace();
					}
				}else
				{
					try{
						PreparedStatement ps = con.prepareStatement(workoutDetails);
						try{
							
							ps.setString(1, userId);
							ps.setString(2, date);
							ps.setString(3, date);
							System.out.println("contents query...."+ps);
							ResultSet rs = ps.executeQuery();
							try{
								while(rs.next()){
									for(col=0;col<12;col++){
										System.out.println(header[col]+"    ");
										System.out.println(rs.getString(header[col]));
										System.out.println(col);
										contents[col][row]=rs.getString(header[col]);
									}
									row++;
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
						con.commit();
					}catch(SQLException e){
						con.rollback();
						e.printStackTrace();
					}
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
      WriteExcel test = new WriteExcel();
      test.setOutputFile(fileName);
      if(count==0){
    	  test.write(header,12,contents,9,row);
      }else {
    	  test.write(header,12,contents,12,row);
      }
      System.out
              .println("Please check the result file under E:/projects/testout.xls");

        
        return fileName;
        }
	
	
	
	@SuppressWarnings("unchecked")
	@Path("/uploadTemplateFile")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String uploadTemplateFile(@CookieParam("uploadtoken") String token,@QueryParam("filename") String fileName,@QueryParam("user_id") String userId) throws IOException, WriteException, ParseException{
		
		String loggedUserId;
		JSONParser parser = new JSONParser();
		JSONObject obj = new JSONObject();
		obj.put("activity_id", "UTFT");
		obj.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		
		ReadExcel test = new ReadExcel();
		String filenameWithPath ="E:/projects/SIX ALPHA/"+fileName;
		System.out.println("here......"+filenameWithPath);
		test.setInputFile(filenameWithPath);
        test.read();
        String contents[][]=test.getcontents();
        System.out.println("returned contents   :"+contents);
        int nrows=test.getrows();
        test.getcols();
        int count=0;
        String type = null;
        
        String validateExistence="select count(*) as count from user_workout_plan_details "
        		+ "where  plan_id=? and user_id=? and template_id= ? and dayno = ? and exercise_id=?";
       
        String updateQuery1="update user_workout_plan_details set duration = ? ,modified_by=? ,modified_date=curdate() where plan_id=? and user_id=? and template_id= ? and dayno = ? and exercise_id=?";
        String updateQuery2="update user_workout_plan_details set repetition = ? , sets = ?,modified_by=? ,modified_date=curdate() where plan_id=? and user_id=? and template_id= ? and dayno = ? and exercise_id=?";
        String insertQuery1="insert into user_workout_plan_details "
        		+ "(plan_id, user_id,template_id,dayno,exercise_id,item_id,intensity,duration,"
        		+ "created_by,created_date,modified_by,modified_date) values "
        		+ "(?,?,?,?,?,?,?,?,?,curdate(),?,curdate())";
        String insertQuery2="insert into user_workout_plan_details "
        		+ "(plan_id, user_id,template_id,dayno,exercise_id,item_id,intensity,repetition,sets,"
        		+ "created_by,created_date,modified_by,modified_date) values "
        		+ "(?,?,?,?,?,?,?,?,?,?,curdate(),?,curdate())";
        
        
        try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				for (int row=1;row<nrows;row++)
				{
				   try{
					   PreparedStatement ps = con.prepareStatement(validateExistence);
					   	try{
					   		
					   		for(int col=0;col<5;col++)
					   		{
					   			ps.setString(col+1, contents[col][row]);
					   		}
					   			ResultSet rs = ps.executeQuery();
						try{
							if(rs.next())
							{
								count =rs.getInt("count");
							}
						}finally{
							rs.close();
						}
					   	}finally{
					   		ps.close();
					   	}
					   	con.commit();
				   		}catch(SQLException e){
				   		 con.rollback();
				   		 e.printStackTrace();
				   			}
				   type = contents[8][row];
				   System.out.println("type.........."+type);
				   if (count==0)
				   {
				   		if(type.equals("1")){
					   try{
						   PreparedStatement ps = con.prepareStatement(insertQuery1);
						   	try{
						   		
						   		for(int col=0;col<7;col++)
						   		{
						   			ps.setString(col+1, contents[col][row]);
						   		}
						   		System.out.println("duration..........."+contents[9][row]);
						   		ps.setString(8,contents[9][row]);
						   		
						   		ps.setString(9, loggedUserId);
						   		ps.setString(10, loggedUserId);
						   		
						   		ps.executeUpdate();
							
						   	}finally{
						   		ps.close();
						   	}
						   	con.commit();
					   		}catch(SQLException e){
					   		 con.rollback();
					   		 e.printStackTrace();
					   			}
				   		}else{
				   			try{
								   PreparedStatement ps = con.prepareStatement(insertQuery2);
								   	try{
								   		
								   		for(int col=0;col<7;col++)
								   		{
								   			ps.setString(col+1, contents[col][row]);
								   		}
								   		System.out.println("rep & sets...."+contents[10][row]+"   "+contents[11][row]);
								   		ps.setString(8,contents[10][row]);
								   		ps.setString(9,contents[11][row]);
								   		ps.setString(10, loggedUserId);
								   		ps.setString(11, loggedUserId);
								   		ps.executeUpdate();
									
								   	}finally{
								   		ps.close();
								   	}
								   	con.commit();
							   		}catch(SQLException e){
							   		 con.rollback();
							   		 e.printStackTrace();
							   			}
				   		}
				   	}else{
				   			if(type.equals("1")){
				   				
				   				try{
									   PreparedStatement ps = con.prepareStatement(updateQuery1);
									   	try{
									   		ps.setString(1, contents[9][row]);
									   		ps.setString(2,loggedUserId);
									   		int i=3;
									   		for(int col=0;col<5;col++)
									   		{
									   			
									   			ps.setString(i, contents[col][row]);
									   			i=i+1;
									   		}
									   		
									   			ps.executeUpdate();
										
									   	}finally{
									   		ps.close();
									   	}
									   	con.commit();
								   		}catch(SQLException e){
								   		 con.rollback();
								   		 e.printStackTrace();
								   			}
				   			}else {
				   				try{
									   PreparedStatement ps = con.prepareStatement(updateQuery2);
									   	try{
									   		ps.setString(1, contents[10][row]);
									   		ps.setString(2, contents[11][row]);
									   		ps.setString(3,loggedUserId);
									   		int i=4;
									   		for(int col=0;col<5;col++)
									   		{
									   			
									   			ps.setString(i, contents[col][row]);
									   			i=i+1;
									   		}
									   			ps.executeUpdate();
										
									   	}finally{
									   		ps.close();
									   	}
									   	con.commit();
								   		}catch(SQLException e){
								   		 con.rollback();
								   		 e.printStackTrace();
								   			}
				   			}
				   	}
				   		
				}  
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileName;
		
}
	
	@SuppressWarnings("unchecked")
	@Path("/getTemplateIds")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String getTemplateIds(@CookieParam("uploadtoken") String token) throws ParseException {
		
		String loggedUserId;
		JSONParser parser = new JSONParser();
		JSONObject obj1 = new JSONObject();
		obj1.put("activity_id", "GTIT");
		obj1.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj1.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		
		String getTemplateIds="select distinct template_id from workout_template";
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				
					try{
						PreparedStatement ps = con.prepareStatement(getTemplateIds);
						try{
							ResultSet rs = ps.executeQuery();
							try{
								while(rs.next()){
									arr.add( rs.getString("template_id"));
								}
								obj.put("template_id", arr);
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println(obj.toString());
		return obj.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	@Path("/getWorkoutPlanHeader")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String getWorkoutPlanHeader(@CookieParam("uploadtoken") String token,@QueryParam("user_id") String userId) throws ParseException {
		
		String loggedUserId;
		JSONParser parser = new JSONParser();
		JSONObject obj1 = new JSONObject();
		obj1.put("activity_id", "GWPHT");
		obj1.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj1.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		
		String getWorkoutPlanHeader="select plan_id,fromdate,todate,template_id from user_workout_plan_header  where user_id=?"
											+ " and todate=(select max(todate) from user_workout_plan_header where user_id=?)";
		JSONObject obj = new JSONObject();
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				
					try{
						PreparedStatement ps = con.prepareStatement(getWorkoutPlanHeader);
						try{
							ps.setString(1, userId);
							ps.setString(2, userId);
							ResultSet rs = ps.executeQuery();
							try{
								if(rs.next()){
									obj.put("plan_id", rs.getString("plan_id"));
									obj.put("fromdate", rs.getString("fromdate"));
									obj.put("todate", rs.getString("todate"));
									obj.put("template_id", rs.getString("template_id"));
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return obj.toString();
		
	}
	
	@SuppressWarnings("unchecked")
	@Path("/getUserDetails")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String getUserDetails(@CookieParam("uploadtoken") String token,@QueryParam("user_id") String userId) throws ParseException {
		
		String loggedUserId;
		JSONParser parser = new JSONParser();
		JSONObject obj1 = new JSONObject();
		obj1.put("activity_id", "UGUDT");
		obj1.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj1.toString());
		JSONObject userAuthjson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthjson.get("Status") || (Boolean) userAuthjson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthjson.get("user_id").toString();
		
		String getUserName="select first_name,last_name from users where user_id=?";
		System.out.println("user id ...."+userId);
		JSONObject obj = new JSONObject();
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				
					try{
						PreparedStatement ps = con.prepareStatement(getUserName);
						try{
							ps.setString(1, userId);
							ResultSet rs = ps.executeQuery();
							try{
								if(rs.next()){
									obj.put("first_name", rs.getString("first_name"));
									obj.put("last_name", rs.getString("last_name"));
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("user details...."+obj.toString());
		return obj.toString();
		
	}
	
	@SuppressWarnings("unchecked")
	@Path("/maintainWorkoutPlanHeader")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String maintainWorkoutPlanHeader(@CookieParam("uploadtoken") String token,@QueryParam("user_id") String userId,@QueryParam("fromdate") String fromDate,
													@QueryParam("todate") String toDate,@QueryParam("template_id") String templateId,@QueryParam("plan_id") String planId) throws ParseException {
		
		String loggedUserId,ouId;
		JSONParser parser = new JSONParser();
		JSONObject obj1 = new JSONObject();
		obj1.put("activity_id", "MWPHT");
		obj1.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj1.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		ouId = userAuthJson.get("ou_id").toString();
		
		String getPlanId="select max(plan_id)+1 as plan_id from user_workout_plan_header";
		String insertPlanValidation="select count(*) as count from user_workout_plan_header where user_id=? and todate>=?";
		String updatePlanValidation="select count(*) as count from user_workout_plan_header where user_id=? and todate>=? and plan_id<?";
		String insertWorkoutPlanHeader="insert into user_workout_plan_header (plan_id,user_id,ou_id,fromdate,todate,template_id,created_date,created_by,modified_date,modified_by)"
				+ "values(?,?,?,?,?,?,curdate(),?,curdate(),?)";
		String updatWorkoutPlanHeader="update user_workout_plan_header set fromdate=?,todate=?,template_id=?,modified_date=curdate(),modified_by=? where user_id=? and ou_id=? and plan_id=?";
		int count=0;
		JSONObject obj = new JSONObject();
		obj.put("value","failure");
		System.out.println("data"+planId+"..."+userId+"..."+fromDate+"..."+toDate+"..."+templateId);
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				if(planId==null)
				{
					try{
						PreparedStatement ps = con.prepareStatement(insertPlanValidation);
						try{
							ps.setString(1, userId);
							ps.setString(2, fromDate);
							ResultSet rs = ps.executeQuery();
							try{
								if(rs.next()){
									 count = rs.getInt("count");
									 System.out.println("insertplanvalid...."+count);
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}if(count==0)
			{
				try{
					   PreparedStatement ps = con.prepareStatement(getPlanId);
					   	try{
					   			
					   			ResultSet rs=ps.executeQuery();
						try{
							if(rs.next()){
								planId=rs.getString("plan_id");
							}
							System.out.println("plan_id....."+planId);
						}finally{
							rs.close();
						}
					   	}finally{
					   		ps.close();
					   	}
					   	con.commit();
					   	
				   		}catch(SQLException e){
				   		 con.rollback();
				   		 e.printStackTrace();
				   		}
				
				try{
					   PreparedStatement ps = con.prepareStatement(insertWorkoutPlanHeader);
					   	try{
					   			ps.setString(1, planId);
					   			ps.setString(2, userId);
					   			ps.setString(3, ouId);
					   			ps.setString(4, fromDate);
					   			ps.setString(5, toDate);
					   			ps.setString(6, templateId);
					   			ps.setString(7, loggedUserId);
					   			ps.setString(8, loggedUserId);
					   			ps.executeUpdate();
						
					   	}finally{
					   		ps.close();
					   	}
					   	con.commit();
					   	
				   		}catch(SQLException e){
				   		 con.rollback();
				   		 e.printStackTrace();
				   		}
					}
				}else{
					try{
						PreparedStatement ps = con.prepareStatement(updatePlanValidation);
						try{
							ps.setString(1, userId);
							ps.setString(2, fromDate);
							ps.setString(3, planId);
							ResultSet rs = ps.executeQuery();
							try{
								if(rs.next()){
									 count = rs.getInt("count");
									 System.out.println("updateplanvalid...."+count);
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}if(count==0)
				{
					try{
						   PreparedStatement ps = con.prepareStatement(updatWorkoutPlanHeader);
						   	try{
						   			
						   			ps.setString(1, fromDate);
						   			ps.setString(2, toDate);
						   			ps.setString(3, templateId);
						   			ps.setString(4, loggedUserId);
						   			ps.setString(5, userId);
						   			ps.setString(6, ouId);
						   			ps.setString(7, planId);
						   			ps.executeUpdate();
							
						   	}finally{
						   		ps.close();
						   	}
						   	con.commit();
						   	
					   		}catch(SQLException e){
					   		 con.rollback();
					   		 e.printStackTrace();
					   		}
				}
			 }	
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		obj.put("status",true);
		obj.put("plan_id",planId);
		System.out.println(obj.toString());
		return obj.toString();
		
	}
	
	@SuppressWarnings({ "unchecked" })
	@Path("/getWorkoutPlanDetails")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String getWorkoutPlanDetails(@CookieParam("uploadtoken") String token,@QueryParam("user_id") String userId,@QueryParam("plan_id") String planId) throws ParseException {
	
		String loggedUserId;
		JSONParser parser = new JSONParser();
		JSONObject obj1 = new JSONObject();
		obj1.put("activity_id", "GWPDT");
		obj1.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj1.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		
		String getCount="select count(*) as count from user_workout_plan_details where user_id=? and plan_id=?";
		String contentsQuery="select dayno,b.exercise_id,b.item_id,"
				+ "c.intensity,c.exercise_name,c.exercise_type from "
				+ "user_workout_plan_header a inner join workout_template b on"
				+ " a.template_id=b.template_id inner join exercise_definition c "
				+ "on b.exercise_id=c.exercise_id where a.user_id=? and plan_id=?";
		String header[] = {"dayno","exercise_id","item_id","intensity",
				"exercise_name", "exercise_type","duration","repetition","sets"};
		String workoutDetails="select dayno,a.exercise_id,a.item_id,"
				+ "b.intensity,b.exercise_name,b.exercise_type,a.duration,a.repetition,a.sets from "
				+ "user_workout_plan_details a  inner join exercise_definition b "
				+ "on a.exercise_id=b.exercise_id where a.user_id=? and a.plan_id=?";

		JSONArray arr = new JSONArray();
		
		int count=0;
		try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				
				 try{
						PreparedStatement ps = con.prepareStatement(getCount);
						try{
							ps.setString(1, userId);
							ps.setString(2, planId);
							ResultSet rs = ps.executeQuery();
							try{
								if(rs.next()){
									count=rs.getInt("count");
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
				 	if(count==0){
				 		try{
							PreparedStatement ps = con.prepareStatement(contentsQuery);
							try{
								
								ps.setString(1, userId);
								ps.setString(2, planId);
								ResultSet rs = ps.executeQuery();
								try{
									int row = 0;
									while(rs.next()){
										JSONObject obj = new JSONObject();
										for(int col=0;col<6;col++){
											System.out.println(header[col]+"    ");
											System.out.println(rs.getString(header[col]));
											obj.put(header[col],rs.getString(header[col]));
										}
										arr.add(obj);
										System.out.println("obj details..."+arr.toString());
									}
								}finally{
									rs.close();
								}
							}finally{
								ps.close();
							}
						con.commit();
					}catch(SQLException e){
						con.rollback();
						e.printStackTrace();
					}
				 	}else{
				 		try{
							PreparedStatement ps = con.prepareStatement(workoutDetails);
							try{
								
								ps.setString(1, userId);
								ps.setString(2, planId);
								ResultSet rs = ps.executeQuery();
								try{
									int row = 0;
									while(rs.next()){
										JSONObject obj = new JSONObject();
										for(int col=0;col<9;col++)
										{
											System.out.println(header[col]+"    ");
											System.out.println(rs.getString(header[col]));
											obj.put(header[col],rs.getString(header[col]));
										}
										arr.add(obj);
										System.out.println("obj details..."+arr.toString());
									}
								}finally{
									rs.close();
								}
							}finally{
								ps.close();
							}
						con.commit();
					}catch(SQLException e){
						con.rollback();
						e.printStackTrace();
					}
				 	}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println(arr.toString());
		return arr.toString();
	}
	
	@SuppressWarnings("unchecked")
	@Path("/grid_submit")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public static String grid_submit(@CookieParam("uploadtoken") String token,@QueryParam("data") String data,@QueryParam("user_id") String userId,@QueryParam("fromdate") String fromDate,
													@QueryParam("todate") String toDate,@QueryParam("template_id") String templateId,@QueryParam("plan_id") String planId) throws ParseException {
		
		System.out.println("Heeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeere");
		String loggedUserId,ouId;
		JSONParser parser = new JSONParser();
		JSONObject obj1 = new JSONObject();
		obj1.put("activity_id", "GST");
		obj1.put("token", token);
		String userAuth = Login.authenticateUserPrivileges(obj1.toString());
		JSONObject userAuthJson=(JSONObject) parser.parse(userAuth);
		if(!(Boolean)userAuthJson.get("Status") || (Boolean) userAuthJson.get("Error")){
			return userAuth;
		}
		loggedUserId = userAuthJson.get("user_id").toString();
		ouId = userAuthJson.get("ou_id").toString();
		
		int count=0;
        String type = null;
        
        String validateExistence="select count(*) as count from user_workout_plan_details "
        		+ "where  plan_id=? and user_id=? and ou_id=? and template_id= ? and dayno = ? and exercise_id=?";
       
        String updateQuery1="update user_workout_plan_details set duration = ? ,modified_by=? ,modified_date=curdate() where plan_id=? and user_id=? and ou_id=? and template_id= ? and dayno = ? and exercise_id=?";
        String updateQuery2="update user_workout_plan_details set repetition = ? , sets = ?,modified_by=? ,modified_date=curdate() where plan_id=? and user_id=? and ou_id=? and template_id= ? and dayno = ? and exercise_id=?";
        String insertQuery1="insert into user_workout_plan_details "
        		+ "(plan_id, user_id,ou_id,template_id,dayno,exercise_id,rest,intensity,duration,"
        		+ "created_by,created_date,modified_by,modified_date) values "
        		+ "(?,?,?,?,?,?,?,?,?,?,curdate(),?,curdate())";
        String insertQuery2="insert into user_workout_plan_details "
        		+ "(plan_id,user_id,ou_id,template_id,dayno,exercise_id,rest,intensity,repetition,sets,"
        		+ "created_by,created_date,modified_by,modified_date) values "
        		+ "(?,?,?,?,?,?,?,?,?,?,?,curdate(),?,curdate())";
        
        
        try{
			Connection con = DBConnection.createConnection_mysql();
			try{
				con.setAutoCommit(false);
				
				JSONArray gjson = (JSONArray) parser.parse(data);
				for(int i=0;i<gjson.size();i++)
				{
					
					JSONObject obj= (JSONObject)gjson.get(i);
					try{
						PreparedStatement ps = con.prepareStatement(validateExistence);
						try{
							ps.setString(1, planId);
							ps.setString(2, userId);
							ps.setString(3, ouId);
							ps.setString(4, templateId);
							ps.setString(5, obj.get("dayno").toString());
							ps.setString(6, obj.get("exercise_id").toString());
							ResultSet rs = ps.executeQuery();
							try{
								if(rs.next()){
									count = rs.getInt("count");
								}
							}finally{
								rs.close();
							}
						}finally{
							ps.close();
						}
					con.commit();
				}catch(SQLException e){
					con.rollback();
					e.printStackTrace();
				}
					if(count!=0)
					{
						type= obj.get("exercise_type").toString();
						if(type.equals("1"))
						{
							try{
								PreparedStatement ps = con.prepareStatement(updateQuery1);
								try{
									ps.setString(1, obj.get("duration").toString());
									ps.setString(2, loggedUserId);
									ps.setString(3, planId);
									ps.setString(4, userId);
									ps.setString(5, ouId);
									ps.setString(6, templateId);
									ps.setString(7, obj.get("dayno").toString());
									ps.setString(8, obj.get("exercise_id").toString());
									ps.executeUpdate();
								}finally{
										ps.close();
										}
										con.commit();
								}catch(SQLException e){
									con.rollback();
									e.printStackTrace();
								}
						}else
						{
							try{
								PreparedStatement ps = con.prepareStatement(updateQuery2);
								try{
									ps.setString(1, obj.get("repetition").toString());
									ps.setString(2, obj.get("sets").toString());
									ps.setString(3, loggedUserId);
									ps.setString(4, planId);
									ps.setString(5, userId);
									ps.setString(6, ouId);
									ps.setString(7, templateId);
									ps.setString(8, obj.get("dayno").toString());
									ps.setString(9, obj.get("exercise_id").toString());
									ps.executeUpdate();
								}finally{
										ps.close();
										}
										con.commit();
								}catch(SQLException e){
									con.rollback();
									e.printStackTrace();
								}
						}
					}else if(count==0)
					{
						type= obj.get("exercise_type").toString();
						if(type.equals("1"))
						{
							try{
								PreparedStatement ps = con.prepareStatement(insertQuery1);
								try{
									ps.setString(1, planId);    
									ps.setString(2, userId);
									ps.setString(3, ouId);
									ps.setString(4, templateId);
									ps.setString(5, obj.get("dayno").toString());
									ps.setString(6, obj.get("exercise_id").toString());
									ps.setString(7, obj.get("item_id").toString());
									ps.setString(8, obj.get("intensity").toString());
									ps.setString(9, obj.get("duration").toString());
									ps.setString(10, loggedUserId);
									ps.setString(11, loggedUserId);						
									ps.executeUpdate();
								}finally{
										ps.close();
										}
										con.commit();
								}catch(SQLException e){
									con.rollback();
									e.printStackTrace();
								}
						}else
						{
							try{
								PreparedStatement ps = con.prepareStatement(insertQuery2);
								try{
									ps.setString(1, planId);
									ps.setString(2, userId);
									ps.setString(3, ouId);
									ps.setString(4, templateId);
									ps.setString(5, obj.get("dayno").toString());
									ps.setString(6, obj.get("exercise_id").toString());
									ps.setString(7, obj.get("item_id").toString());
									ps.setString(8, obj.get("intensity").toString());
									ps.setString(9, obj.get("repetition").toString());
									ps.setString(10, obj.get("sets").toString());
									ps.setString(11, loggedUserId);
									ps.setString(12, loggedUserId);						
									ps.executeUpdate();
								}finally{
										ps.close();
										}
										con.commit();
								}catch(SQLException e){
									con.rollback();
									e.printStackTrace();
								}
						}
					}
			}
			}finally{
				con.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
        return null;
	}
}
